use itertools::Itertools;
use proc_macro::TokenStream as TStream;
use proc_macro2::TokenStream;
use quote::{format_ident, quote};
use syn::{
    parse_macro_input, parse_quote,
    visit_mut::{self, VisitMut},
    Attribute, Error, FnArg, GenericParam, ItemTrait, Pat, Path, Receiver, Token,
    TraitBoundModifier, TraitItem, Type, TypeParamBound, TypePath, WherePredicate,
};

fn remove_sized_attr(attrs: &mut Vec<Attribute>) -> (bool, Vec<Error>) {
    let mut errors = Vec::new();

    let mut has_sized = false;
    attrs.retain(|attr| {
        if attr.path().is_ident("Sized") {
            if has_sized {
                errors.push(Error::new_spanned(
                    attr,
                    "The Sized attribute can only be applied once.",
                ));
            } else if let Err(err) = attr.meta.require_path_only() {
                errors.push(err);
            }

            has_sized = true;

            false
        } else {
            true
        }
    });

    (has_sized, errors)
}

fn has_sized_attr(attrs: &[Attribute]) -> (bool, Vec<Error>) {
    let mut errors = Vec::new();

    let mut has_sized = false;
    for attr in attrs {
        if attr.path().is_ident("Sized") {
            if has_sized {
                errors.push(Error::new_spanned(
                    attr,
                    "The Sized attribute can only be applied once.",
                ));
            } else if let Err(err) = attr.meta.require_path_only() {
                errors.push(err);
            }

            has_sized = true;
        }
    }

    (has_sized, errors)
}

#[proc_macro_attribute]
pub fn veritable(args: TStream, input: TStream) -> TStream {
    let mut errors = Vec::<Error>::new();

    let mut trait_def = parse_macro_input!(input as ItemTrait);

    let emit = args.is_empty();
    let trait_path = if emit {
        let ident = &trait_def.ident;
        parse_quote!(#ident)
    } else {
        parse_macro_input!(args as Path)
    };

    let mod_name = format_ident!("{}", trait_def.ident.to_string().to_lowercase());

    let mut generics = trait_def.generics.clone();
    ReplaceSelf.visit_generics_mut(&mut generics);

    let (veritable_trait_item_defs_tokens, err) = veritable_trait_item_defs(&trait_def);
    errors.extend(err);

    let (veritable_trait_item_impl_with_trait_tokens, err) =
        veritable_trait_item_impl_with_trait(&trait_def);
    errors.extend(err);

    let (veritable_trait_item_impl_without_trait_tokens, err) =
        veritable_trait_item_impl_without_trait(&trait_def);
    errors.extend(err);

    let generic_ident_list_tokens = generic_ident_list(&trait_def, true);
    let generic_idents = generic_ident_list(&trait_def, false);
    let generic_markers = generic_marker_types(&trait_def);

    let vis = &trait_def.vis;
    let gen = quote! {
        #vis mod #mod_name {
            pub struct Witness<Required, #generic_idents: ?Sized>(::core::marker::PhantomData<(Required, #generic_markers)>);

            // pub trait Veritable #generics: ::veritables::ForType {
            pub trait Veritable<Rhs: ?Sized = <Self as ::veritables::ForType>::T>: ::veritables::ForType {
                #veritable_trait_item_defs_tokens
            }

            pub struct InfoImpl<T: ?Sized, Required, #generic_idents>(::core::marker::PhantomData<(fn() -> *const T, Required, #generic_markers)>);

            impl<T: ?Sized, Required: ::veritables::Bool, #generic_idents> ::veritables::ForType for InfoImpl<T, Required, #generic_idents> {
                type T = T;
            }

            impl<T: ?Sized + #trait_path #generic_ident_list_tokens, #generic_idents> Veritable<#generic_idents> for InfoImpl<T, ::veritables::True, #generic_idents> {
                #veritable_trait_item_impl_with_trait_tokens
            }

            impl<T: ?Sized, #generic_idents> Veritable<#generic_idents> for InfoImpl<T, ::veritables::False, #generic_idents> {
                #veritable_trait_item_impl_without_trait_tokens
            }
        }
    };

    let errors: TokenStream = errors
        .into_iter()
        .map(|err| err.into_compile_error())
        .collect();

    if emit {
        quote!(#trait_def #gen #errors).into()
    } else {
        quote!(#gen #errors).into()
    }
}

/// Definitions of the `Varitable` trait items.
fn veritable_trait_item_defs(trait_def: &ItemTrait) -> (TokenStream, Vec<Error>) {
    let mut errors = Vec::new();

    let stream = trait_def
        .items
        .iter()
        .map(|item| match item {
            TraitItem::Fn(item) => {
                // Make a clone of the function signature we can change.
                let mut sig = item.sig.clone();

                // Force the function to be unsafe.
                // We are adding a safety condition to be able to call it.
                sig.unsafety = Some(Default::default());

                // Replace all uses of `Self` with `Self::T`.
                ReplaceSelf.visit_signature_mut(&mut sig);

                // Check if the user marked the function with `#[Sized]`.
                let (has_sized, err) = has_sized_attr(&item.attrs);
                errors.extend(err);

                // When marked Sized we need to add bounds to the function so the trait
                // still works for `T: !Sized`.
                if has_sized {
                    match &mut sig.generics.where_clause {
                        Some(clause) => clause.predicates.push(parse_quote!(Self::T: Sized)),
                        None => {
                            sig.generics.where_clause = Some(parse_quote!(where Self::T: Sized))
                        }
                    }
                }

                // Emit the function signature.
                quote! {
                    #sig;
                }
            }
            TraitItem::Type(item) => {
                // Make a clone of the type item we can change.
                let mut item = item.clone();

                // Remove all attributes from it.
                item.attrs.clear();

                // Emit the item.
                quote!(#item)
            }
            _ => quote!(),
        })
        .collect();

    (stream, errors)
}

fn veritable_trait_item_impl_with_trait(trait_def: &ItemTrait) -> (TokenStream, Vec<Error>) {
    let mut errors = Vec::new();

    let stream = trait_def
        .items
        .iter()
        .map(|item| match item {
            TraitItem::Fn(item) => {
                // Make a clone of the function signature we can change.
                let mut sig = item.sig.clone();

                // Force the function to be unsafe.
                // We are adding a safety condition to be able to call it.
                sig.unsafety = Some(Default::default());

                // Replace all uses of `Self` with `Self::T`.
                ReplaceSelf.visit_signature_mut(&mut sig);

                // Check if the user marked the function with `#[Sized]`.
                let (has_sized, err) = has_sized_attr(&item.attrs);
                errors.extend(err);

                // When marked Sized we need to add bounds to the function so the trait
                // still works for `T: !Sized`.
                if has_sized {
                    match &mut sig.generics.where_clause {
                        Some(clause) => clause.predicates.push(parse_quote!(Self::T: Sized)),
                        None => {
                            sig.generics.where_clause = Some(parse_quote!(where Self::T: Sized))
                        }
                    }
                }

                let name = &sig.ident;
                let inputs: TokenStream = Itertools::intersperse_with(
                    sig.inputs.iter().map(|input| match input {
                        FnArg::Receiver(_) => {
                            unreachable!("All receivers should be replaced with `Self::T`.")
                        }
                        FnArg::Typed(typed) => match &*typed.pat {
                            Pat::Ident(pat) => {
                                let ident = &pat.ident;
                                quote!(#ident)
                            }
                            _ => {
                                errors.push(Error::new_spanned(
                                    &typed.pat,
                                    "Only identifier patterns are allowed.",
                                ));
                                quote!()
                            }
                        },
                    }),
                    || quote!(,),
                )
                .collect();

                // Emit the function signature.
                quote! {
                    #sig {
                        T::#name(#inputs)
                    }
                }
            }
            TraitItem::Type(item) => {
                let ident = &item.ident;

                // Emit the item.
                quote!(type #ident = T::#ident;)
            }
            _ => quote!(),
        })
        .collect();

    (stream, errors)
}

fn veritable_trait_item_impl_without_trait(trait_def: &ItemTrait) -> (TokenStream, Vec<Error>) {
    let mut errors = Vec::new();

    let stream = trait_def
        .items
        .iter()
        .map(|item| match item {
            TraitItem::Fn(item) => {
                // Make a clone of the function signature we can change.
                let mut sig = item.sig.clone();

                // Force the function to be unsafe.
                // We are adding a safety condition to be able to call it.
                sig.unsafety = Some(Default::default());

                // Replace all uses of `Self` with `Self::T`.
                ReplaceSelf.visit_signature_mut(&mut sig);

                // Check if the user marked the function with `#[Sized]`.
                let (has_sized, err) = has_sized_attr(&item.attrs);
                errors.extend(err);

                // When marked Sized we need to add bounds to the function so the trait
                // still works for `T: !Sized`.
                if has_sized {
                    match &mut sig.generics.where_clause {
                        Some(clause) => clause.predicates.push(parse_quote!(Self::T: Sized)),
                        None => {
                            sig.generics.where_clause = Some(parse_quote!(where Self::T: Sized))
                        }
                    }
                }

                let name = &sig.ident;
                let inputs: TokenStream = Itertools::intersperse_with(
                    sig.inputs.iter().map(|input| match input {
                        FnArg::Receiver(_) => {
                            unreachable!("All receivers should be replaced with `Self::T`.")
                        }
                        FnArg::Typed(typed) => match &*typed.pat {
                            Pat::Ident(pat) => {
                                let ident = &pat.ident;
                                quote!(#ident)
                            }
                            _ => {
                                errors.push(Error::new_spanned(
                                    &typed.pat,
                                    "Only identifier patterns are allowed.",
                                ));
                                quote!()
                            }
                        },
                    }),
                    || quote!(,),
                )
                .collect();

                // Emit the function signature.
                quote! {
                    #sig {
                        unreachable!()
                    }
                }
            }
            TraitItem::Type(item) => {
                let ident = &item.ident;

                // Emit the item.
                quote!(type #ident = ::veritables::Nothing;)
            }
            _ => quote!(),
        })
        .collect();

    (stream, errors)
}

fn generic_marker_types(trait_def: &ItemTrait) -> TokenStream {
    if !trait_def.generics.params.is_empty() {
        Itertools::intersperse_with(
            trait_def
                .generics
                .params
                .iter()
                .filter_map(|param| match param {
                    GenericParam::Lifetime(_) => None,
                    GenericParam::Type(ty) => {
                        let ident = &ty.ident;
                        Some(quote!(fn() -> *const #ident))
                    }
                    GenericParam::Const(_) => None,
                }),
            || quote!(,),
        )
        .collect()
    } else {
        quote!()
    }
}

fn generic_ident_list(trait_def: &ItemTrait, include_brackets: bool) -> TokenStream {
    if !trait_def.generics.params.is_empty() {
        let list: TokenStream = Itertools::intersperse_with(
            trait_def.generics.params.iter().map(|param| match param {
                GenericParam::Lifetime(lt) => {
                    let lt = &lt.lifetime;
                    quote!(#lt)
                }
                GenericParam::Type(ty) => {
                    let ident = &ty.ident;
                    quote!(#ident)
                }
                GenericParam::Const(ct) => {
                    let ident = &ct.ident;
                    quote!(#ident)
                }
            }),
            || quote!(,),
        )
        .collect();

        if include_brackets {
            quote!(<#list>)
        } else {
            list
        }
    } else {
        quote!()
    }
}

fn bound_self(needs_sized: bool, bounds: Option<TokenStream>) -> TokenStream {
    match (needs_sized, bounds) {
        (true, None) => quote!(),
        (true, Some(bounds)) => quote!(: #bounds),
        (false, None) => quote!(: ?::core::marker::Sized),
        (false, Some(bounds)) => quote!(: ?::core::marker::Sized + #bounds),
    }
}

struct ReplaceSelf;

impl VisitMut for ReplaceSelf {
    fn visit_fn_arg_mut(&mut self, node: &mut FnArg) {
        visit_mut::visit_fn_arg_mut(self, node);

        match node {
            FnArg::Receiver(receiver) => {
                let mut ty = receiver.ty.clone();
                visit_mut::visit_type_mut(self, &mut ty);

                *node = parse_quote!(this: #ty);
            }
            FnArg::Typed(_) => {}
        }
    }

    fn visit_type_path_mut(&mut self, node: &mut TypePath) {
        if node.qself.is_none() && node.path.is_ident("Self") {
            *node = parse_quote!(<Self as ::veritables::ForType>::T);
            // *node = parse_quote!(Self::T);
        }
    }
}

fn is_self_type_path(path: &TypePath) -> bool {
    path == &parse_quote!(Self)
}

#[cfg(test)]
mod test {
    use super::*;
}
