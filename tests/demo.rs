use std::marker::PhantomData;

use veritables::demo::Bind2;
use veritables::{veritable, Bind, Bool, False, ForType, HasInfo, InfoFor, True, __};

// #[veritable(std::ops::Add)]
// #[bound(Self: Sized)]
// pub trait Add<Rhs = Self> {
//     /// The resulting type after applying the `+` operator.
//     type Output;
//
//     /// Performs the `+` operation.
//     ///
//     /// # Example
//     ///
//     /// ```
//     /// assert_eq!(12 + 1, 13);
//     /// ```
//     #[Sized]
//     fn add(self, rhs: Rhs) -> Self::Output;
// }

use veritables::add::add;

// bound: Info<This>
// <bound<... generics ...> as Info<This>>::Info: Trait::Info
// <<bound<... generics ...> as Info<This>>::Info as Trait::Info>::Veritable: Trait::

// A group of `ops` bounds.
struct Ops<A: add::Bound> {
    _marker: PhantomData<A>,
}

// A `T` can be bound by `Ops`.
impl<T: ?Sized, A: add::Bound> HasInfo<T> for Ops<A> {
    type Info = OpsInfo<T, A>;
}

// The info for a bind by `Ops`.
struct OpsInfo<T: ?Sized, A: add::Bound> {
    _marker: PhantomData<(A, fn() -> *const T)>,
}

// `T` is the type.
impl<T: ?Sized, A: add::Bound> ForType for OpsInfo<T, A> {
    type T = T;
}

// Allows using `OpsInfo` where a `add::Info` is needed.
// aka `Ops` knows about `add`.
//
// The trait bound inside `add` is smuggled via the `add::BoundInfo<T>`.
// That will fail if the bound needs `T` to implement `Add` but it doesn't.
impl<T /*: ?Sized*/, A: add::Bound + add::BoundInfo<T>> add::Info for OpsInfo<T, A> {
    type Veritable = <A as add::BoundInfo<T>>::Veritable;
}

struct Demo<T> {
    vtable: add::VTable<T, T, T>,
    value: T,
}

impl<T> Demo<T> {
    fn new<Bound>(value: T) -> Self
    where
        T: Bind<Bound>,
        InfoFor<T, Bound>: add::Info,
        add::VeritableFor<InfoFor<T, Bound>>: add::Veritable<GenericRhs = T, AssociatedOutput = T>,
    {
        Self {
            vtable: add::vtable::<InfoFor<T, Bound>>(),
            value,
        }
    }

    fn do_add(self, other: T) -> Option<T> {
        self.vtable.checked_add(self.value, other)
    }
}

pub fn test<L: HList<Bound>, Bound: veritables::demo::Bindable>() {
    if L::LEN == 0 {
        return;
    }

    // dbg!(<L::Head as Bind<add::bound::RequireWithAssociated<i32, i32>>>::Info);
    veritables::demo::Veritable::<L::Head, Bound>::demo(Default::default());

    test::<L::Tail, Bound>();
    // ignore the fact it doesn't terminate
}

pub trait HList<Bound> {
    const LEN: usize;
    type Head: Bind2<Bound> + Default;
    type Tail: HList<Bound>;
}

impl<Bound> HList<Bound> for ()
where
    (): Bind2<Bound>,
{
    const LEN: usize = 0;

    type Head = ();

    type Tail = ();
}

impl<H: Default + Bind2<Bound>, T: HList<Bound>, Bound> HList<Bound> for (H, T) {
    const LEN: usize = T::LEN + 1;

    type Head = H;

    type Tail = T;
}

mod test2 {
    use veritables::demo2::{Bind, BindDemo};
    use veritables::demo2::{SafeBindDemo, VTableFor};
    use veritables::{Bool, True};

    pub fn test2<T: ?Sized + Bind<Bound>, Bound: BindDemo>(x: &T) -> Bound::Output<T> {
        if <Bound as BindDemo>::Required::VALUE {
            unsafe { <Bound as BindDemo>::VTable::<T>::VTABLE.demo(x) }
        } else {
            panic!("the bound didn't have demo")
        }
    }

    pub fn test3<T: ?Sized + Bind<Bound>, Bound: BindDemo<Required = True>>(
        x: &T,
    ) -> Bound::Output<T> {
        // We can use this because we require Required = True
        SafeBindDemo::<T, Bound>::demo(x)
    }
}

#[test]
fn demo() {
    let mut x: Demo<i32>;

    x = Demo::new::<Ops<add::bound::DontRequireWithAssociated<_, _>>>(54);
    dbg!(&x.vtable);

    x = Demo::new::<Ops<add::bound::RequireWithAssociated<_, _>>>(54);
    dbg!(&x.vtable);

    x = Demo::new::<Ops<add::bound::Require<_>>>(54);
    dbg!(&x.vtable);

    x = Demo::new::<Ops<add::bound::Toggle<False, _, _>>>(54);
    dbg!(&x.vtable);

    // struct NotDebug;
    test::<(i32, (f32, (String, ()))), veritables::demo::X>();

    #[derive(Debug)]
    struct A<'a> {
        x: &'a i32,
    }

    let y = 54;
    let z = A { x: &y };

    dbg!(test2::test2::<_, veritables::demo2::Required>(&z));
    dbg!(test2::test2::<_, veritables::demo2::Required>("test"));

    dbg!(test2::test3::<_, veritables::demo2::Required>("test"));
    // dbg!(test2::<_, veritables::demo2::NotRequired>(&z));

    todo!()
}
