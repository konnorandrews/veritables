pub mod add;

use std::{
    marker::PhantomData,
    panic::{RefUnwindSafe, UnwindSafe},
};

pub use veritables_macros::veritable;

pub trait Bind<Bound: ?Sized> {
    type Info: ForType<T = Self>;
}

impl<T: ?Sized, Bound: ?Sized + HasInfo<T>> Bind<Bound> for T {
    type Info = <Bound as HasInfo<T>>::Info;
}

// // `Self` is boundable by `bound`.
// pub trait Bind2<Bound> {
//
// }
//
// pub struct Impl<T: ?Sized, Bound> {
//     _marker: PhantomData<(
//         fn() -> (
//             *const T,
//             Bound,
//         ),
//     )>,
// }
//
// // When bound to `T` provide the veritable.
// trait AddBindable {
//     type Info<T: Bind2<Self>>: Veritable<T = T>;
// }
//
// trait AddBound<T> {
//     type Info: Veritable<T = T>;
// }
//
// trait Veritable: ForType where Self::T: Sized {
//     fn demo(x: Self::T);
// }
//
// impl<T: ?Sized, Bound> ForType
//     for Impl<T, Bound>
// {
//     type T = T;
// }
//
// struct X;
//
// impl AddBindable for X {
//     type Info<T: Bind2<Self>> = Impl<T, Self>;
// }
//
// impl<T: std::fmt::Debug> Bind2<X> for T {}
//
// impl<T: std::fmt::Debug, Bound: AddBindable> Veritable for Impl<T, Bound>
// {
//     fn demo(x: Self::T) {
//         todo!()
//     }
// }

pub trait Demo {
    fn demo(self);
}

impl<T: std::fmt::Debug> Demo for T {
    fn demo(self) {
        dbg!(self);
    }
}

pub trait Demo2 {
    type Output;

    fn demo(&self) -> Self::Output;
}

impl<T: ?Sized + std::fmt::Debug> Demo2 for T {
    type Output = i32;

    fn demo(&self) -> Self::Output {
        dbg!(self);
        42
    }
}

pub mod demo2 {
    use std::{marker::PhantomData, mem::ManuallyDrop};

    use crate::{Bool, False, IsRequired, True};

    use super::Demo2;

    /// If `T: Bind<Bound>`, this is equivalent to `T: Bound`.
    pub trait Bind<Bound> {
        /// This is used to forward info about the bound for use by other traits.
        type Info;

        type AssociatedA;
        type AssociatedB;
        type AssociatedC;

        /// Instance of the vtable to use to access methods.
        const INFO: Self::Info;
    }

    pub trait HasInfo<T: ?Sized> {
        type Info;

        type AssociatedA;
        type AssociatedB;
        type AssociatedC;

        const INFO: Self::Info;
    }

    // SAFETY: This must be the only impl of Bind
    impl<T: ?Sized, Bound> Bind<Bound> for T
    where
        Bound: HasInfo<T>,
    {
        const INFO: Self::Info = <Bound as HasInfo<T>>::INFO;

        type AssociatedA = <Bound as HasInfo<T>>::AssociatedA;
        type AssociatedB = <Bound as HasInfo<T>>::AssociatedB;
        type AssociatedC = <Bound as HasInfo<T>>::AssociatedC;

        type Info = Bound::Info;
    }

    pub const unsafe fn const_transmute<T, U>(value: T) -> U {
        #[repr(C)]
        union Transmute<T, U> {
            t: ManuallyDrop<T>,
            u: ManuallyDrop<U>,
        }

        let value = Transmute {
            t: ManuallyDrop::new(value),
        };
        ManuallyDrop::into_inner(unsafe { value.u })
    }

    // === === ===

    // Trait for if a type encoded bound can bind [`Demo2`].
    pub trait BindDemo: Sized {
        // If [`Demo2`] was reuqired to be implemented.
        type Required: Bool;

        // The associated type [`Demo2::Output`].
        //
        // Allows bounding on the associated type like with normal trait bounds.
        // The associated type can also by given directly in the bound.
        type Output<T: ?Sized + Bind<Self>>;

        // Can get the vtable for a `T` according to the bound.
        type VTable<T: ?Sized + Bind<Self>>: VTableFor<T, Self>;
    }

    /// Safe access to the vtable methods if `Required = True`.
    pub struct SafeBindDemo<This: ?Sized, Bound> {
        _marker: PhantomData<fn() -> (*const This, Bound)>,
    }

    impl<This: ?Sized + Bind<Bound>, Bound: BindDemo<Required = True>> SafeBindDemo<This, Bound> {
        pub fn demo(this: &This) -> Bound::Output<This> {
            unsafe { Bound::VTable::<This>::VTABLE.demo(this) }
        }
    }

    // How to get a vtable instance from the info of the bound.
    pub trait VTableFor<This: ?Sized + Bind<Bound>, Bound: BindDemo> {
        const VTABLE: VTable<This, Bound::Output<This>>;
    }

    // VTable with all the methods of [`Demo2`].
    pub struct VTable<This: ?Sized, Output> {
        required: bool,
        demo: unsafe fn(x: &This) -> Output,
    }

    impl<This: ?Sized, Output> VTable<This, Output> {
        /// Create vtable with [`Demo2`] required.
        pub const fn required() -> Self
        where
            This: Demo2<Output = Output>,
        {
            Self {
                required: true,
                demo: This::demo,
            }
        }

        /// Create vtable with [`Demo2`] not required.
        pub const fn not_required() -> Self {
            Self {
                required: false,
                demo: |_| unimplemented!(),
            }
        }

        pub const fn was_required(&self) -> bool {
            self.required
        }

        pub unsafe fn demo(&self, this: &This) -> Output {
            (self.demo)(this)
        }
    }

    /// Bound that requires [`Demo2`].
    pub struct Required;

    impl<T: ?Sized + Demo2> HasInfo<T> for Required {
        // We can just use the vtable directly as thats all the info we need.
        //
        // This is only used in `VTableFor`.
        type Info = VTable<T, T::Output>;

        // Inject the associated type so that we can use it in `Bindable`.
        //
        // This allows `T` to say what Output is.
        type AssociatedA = T::Output;

        type AssociatedB = ();

        type AssociatedC = ();

        // This is where the vtable is actually made.
        const INFO: Self::Info = VTable::required();
    }

    // We can transmute the INFO as it must be the one from the above impl.
    impl<T: ?Sized + Bind<Self>> VTableFor<T, Self> for Required {
        const VTABLE: VTable<T, <Self as BindDemo>::Output<T>> =
            unsafe { const_transmute(<T as Bind<Self>>::INFO) };
    }

    impl BindDemo for Required {
        // We required the trait.
        type Required = True;

        // Grab the associated type that got forwarded.
        type Output<T: ?Sized + Bind<Self>> = <T as Bind<Self>>::AssociatedA;

        // This type knows how to get the vtable for itself.
        type VTable<T: ?Sized + Bind<Self>> = Self;
    }

    // A bound implements N `Bindable`.

    // pub struct NotRequired;
    //
    // impl Bindable for NotRequired {
    //     type Required = False;
    //
    //     type Output<T: ?Sized + Bind<Self>> = ();
    //
    //     type Impl<T: ?Sized + Bind<Self>> = Self;
    // }
    //
    // // This is where the magic happens.
    // // The `T: Bind<Self>` can only have the correct `Info` to make this work.
    // impl<T: ?Sized + Bind<Self>> Impl<T, ()> for NotRequired {
    //     type Required = False;
    //
    //     unsafe fn demo(x: &T) {
    //         let info = <T as Bind<Self>>::INFO;
    //
    //         // Do we know that info must be a dyn VTable<...>?
    //         // The only impl has type Info = RequiredImpl<T>;
    //         // and that has type VTable<'a> = dyn VTable<This, This::Output> + 'a;
    //
    //         unsafe { info.as_vtable::<dyn VTable<T, ()>>().demo(x) }
    //     }
    // }
    //
    // pub struct NotRequiredImpl<This: ?Sized> {
    //     _marker: PhantomData<fn() -> (*const This,)>
    // }
    //
    // impl<This: ?Sized> VTable<This, ()> for NotRequiredImpl<This> {
    //     unsafe fn demo(&self, _x: &This) {
    //         unimplemented!();
    //     }
    // }
    //
    // impl<This: ?Sized> AsVTable for NotRequiredImpl<This> {
    //     type VTable<'a> = dyn VTable<This, ()> + 'a;
    //
    //     unsafe fn as_vtable<'b, T: ?Sized + 'b>(&'b self) -> &'b T {
    //         let vtable = self as &Self::VTable<'b>;
    //         const_transmute(vtable)
    //     }
    // }
    //
    // impl<T: ?Sized + Demo2> HasInfo<T> for NotRequired {
    //     type Info = NotRequiredImpl<T>;
    //
    //     type AssociatedA = ();
    //
    //     type AssociatedB = ();
    //
    //     type AssociatedC = ();
    //
    //     const INFO: Self::Info = NotRequiredImpl {
    //         _marker: PhantomData,
    //     };
    // }
}

pub mod demo {
    use std::{
        any::{Any, TypeId},
        mem::ManuallyDrop,
    };

    use super::*;

    pub trait UpCast {
        fn up_cast<T: Any + ?Sized>(&self) -> &T;
    }

    pub trait Bind2<Bound>: 'static {
        type Info: UpCast;

        const INFO: Self::Info;
    }

    pub struct Veritable<T, Bound> {
        _marker: PhantomData<(T, Bound)>,
    }

    pub trait Demo2: ForType {
        fn demo(x: Self::T);
    }

    pub trait VTable: ForType {
        fn demo(&self, x: Self::T);
    }

    pub trait Bindable {
        type Apply<T: Bind2<Self> + 'static>: Demo2<T = T>;
    }

    pub struct Y<T>(T);

    impl<T> ForType for Y<T> {
        type T = T;
    }

    impl<T: Bind2<X> + 'static> Demo2 for Y<T> {
        fn demo(x: Self::T) {
            T::INFO.up_cast::<dyn VTable<T = T>>().demo(x)
        }
    }

    pub struct X;

    pub struct ImplX<T> {
        _marker: PhantomData<T>,
    }

    impl<T> ForType for ImplX<T> {
        type T = T;
    }

    impl<T: Demo> VTable for ImplX<T> {
        fn demo(&self, x: T) {
            x.demo()
        }
    }

    pub const unsafe fn const_transmute<T, U>(value: T) -> U {
        #[repr(C)]
        union Transmute<T, U> {
            t: ManuallyDrop<T>,
            u: ManuallyDrop<U>,
        }

        let value = Transmute {
            t: ManuallyDrop::new(value),
        };
        ManuallyDrop::into_inner(unsafe { value.u })
    }

    impl<T: Demo + 'static> UpCast for ImplX<T> {
        fn up_cast<U: Any + ?Sized>(&self) -> &U {
            if TypeId::of::<U>() == TypeId::of::<dyn VTable<T = T>>() {
                unsafe {
                    let x: *const U = const_transmute(self as &dyn VTable<T = T> as *const _);
                    &*x
                }
            } else {
                panic!()
            }
        }
    }

    impl<T: Demo + 'static> Bind2<X> for T {
        type Info = ImplX<T>;

        const INFO: Self::Info = ImplX {
            _marker: PhantomData,
        };
    }

    impl Bindable for X {
        type Apply<T: Bind2<Self> + 'static> = Y<T>;
    }

    impl<T: Bind2<Bound>, Bound: Bindable> Veritable<T, Bound> {
        pub fn demo(x: T) {
            <Bound as Bindable>::Apply::<T>::demo(x)
        }
    }
}

// Bound: add::Bound
// T: Bind<Bound>
//
// imply \/
//
// add::Veritable::<T, Bound> exists

pub trait HasInfo<T: ?Sized> {
    type Info: ForType<T = T>;
}

pub enum __ {}

// pub type InfoFor<T, Bound> = <Bound as Info<T>>::Info;
pub type InfoFor<T, Bound> = <T as Bind<Bound>>::Info;

pub trait ForType {
    type T: ?Sized;
}

#[derive(Debug, Clone, Copy)]
pub enum Nothing {}

pub enum True {}
pub enum False {}

pub trait Bool: Send + Sync + Unpin + RefUnwindSafe + UnwindSafe + sealed::Sealed {
    const VALUE: bool;
}

impl Bool for True {
    const VALUE: bool = true;
}

impl Bool for False {
    const VALUE: bool = false;
}

pub trait IsRequired {
    type Required: Bool;
}

mod sealed {
    use super::*;

    pub trait Sealed {}

    impl Sealed for True {}
    impl Sealed for False {}
}
