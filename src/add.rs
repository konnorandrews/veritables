// pub trait Add<Rhs = Self> {
//     type Output;
//
//     fn add(self, rhs: Rhs) -> Self::Output;
// }

/// Veritable enabled form of [`std::ops::Add`].
pub mod add {
    use std::{
        marker::PhantomData,
        panic::{RefUnwindSafe, UnwindSafe},
    };

    // Pull in shared stuff from the veritable crate.
    use crate::{Bool, False, ForType, HasInfo, IsRequired, Nothing, True};

    /// Trait implemented by all types that can bound by just [`std::ops::Add`].
    pub trait Bound: Sized + Send + Sync + Unpin + RefUnwindSafe + UnwindSafe {}

    /// Get the info for a [`Bound`] for some `T`.
    pub trait BoundInfo<T: ?Sized>: Bound {
        /// The veritable describing [`std::ops::Add`] and how `T` implements it.
        type Veritable: Veritable<T = T>;
    }

    /// Bound types for just this trait.
    pub mod bound {
        use super::*;

        /// The shared [`__`][crate::__] type is a valid bound for [`std::ops::Add`].
        ///
        /// Using [`__`][crate::__] does not require `T` implements [`std::ops::Add`].
        /// All generics and associated types are set to [`Nothing`].
        impl Bound for crate::__ {}

        impl<T /*: ?Sized*/> BoundInfo<T> for crate::__ {
            type Veritable = Impl<T, False, Nothing, Nothing>;
        }

        /// Don't require `T` implements [`std::ops::Add`] and allow giving the generics.
        ///
        /// The associated types are set to [`Nothing`].
        pub type DontRequire<Rhs> = Toggle<False, Rhs, Nothing>;

        /// Don't require `T` implements [`std::ops::Add`] and allow giving the generics
        /// and associated types.
        pub type DontRequireWithAssociated<GenericRhs, AssociatedOutput> =
            Toggle<False, GenericRhs, AssociatedOutput>;

        /// Require `T` implements [`std::ops::Add`].
        ///
        /// This doesn't require giving the associated types explicitly.
        pub struct Require<Rhs: ?Sized> {
            _marker: PhantomData<fn() -> (*const Rhs,)>,
        }

        impl<Rhs /*: ?Sized*/> Bound for Require<Rhs> {}

        impl<T /*: ?Sized*/, GenericRhs /*: ?Sized*/> BoundInfo<T> for Require<GenericRhs>
        where
            T: ::core::ops::Add<GenericRhs>,
        {
            type Veritable = Impl<T, True, GenericRhs, <T as ::core::ops::Add<GenericRhs>>::Output>;
        }

        impl<T /*: ?Sized*/, GenericRhs /*: ?Sized*/> HasInfo<T> for Require<GenericRhs>
        where
            T: ::core::ops::Add<GenericRhs>,
        {
            type Info = Impl<T, True, GenericRhs, <T as ::core::ops::Add<GenericRhs>>::Output>;
        }

        /// Require `T` implements [`std::ops::Add`] and allow giving the associated types.
        pub type RequireWithAssociated<GenericRhs, AssociatedOutput> =
            Toggle<True, GenericRhs, AssociatedOutput>;

        /// Type level toggleable requirement.
        ///
        /// When `Required` is [`True`], then `T` is required to implement
        /// [`std::ops::Add`].
        /// When `Required` is [`False`], then `T` is **not** required to implement
        /// [`std::ops::Add`].
        pub struct Toggle<Required: Bool, GenericRhs: ?Sized, AssociatedOutput: ?Sized> {
            _marker: PhantomData<(fn() -> (Required, *const GenericRhs, *const AssociatedOutput),)>,
        }

        impl<Required: Bool, GenericRhs: ?Sized, AssociatedOutput: ?Sized> Bound
            for Toggle<Required, GenericRhs, AssociatedOutput>
        {
        }

        impl<T: ?Sized, Required: Bool, GenericRhs: ?Sized, AssociatedOutput: ?Sized> BoundInfo<T>
            for Toggle<Required, GenericRhs, AssociatedOutput>
        where
            Impl<T, Required, GenericRhs, AssociatedOutput>: Veritable<T = T>,
        {
            type Veritable = Impl<T, Required, GenericRhs, AssociatedOutput>;
        }

        impl<T: ?Sized, Required: Bool, GenericRhs: ?Sized, AssociatedOutput: ?Sized> HasInfo<T>
            for Toggle<Required, GenericRhs, AssociatedOutput>
        where
            Impl<T, Required, GenericRhs, AssociatedOutput>: Veritable<T = T>,
        {
            type Info = Impl<T, Required, GenericRhs, AssociatedOutput>;
        }
    }

    /// Get the [`Veritable`] type for some `I` that implements [`Info`].
    pub type VeritableFor<I> = <I as Info>::Veritable;

    /// Get the [`VTable`] type for some `I` that implements [`Info`].
    pub type VTableFor<I> = VTable<This<I>, generic::Rhs<I>, associated::Output<I>>;

    /// Get the `Self` type for some `I` that implements [`Info`].
    pub type This<I> = <<I as Info>::Veritable as ForType>::T;

    /// Helpers to get the generic types for the trait.
    pub mod generic {
        use super::*;

        /// Get the `Rhs` generic type for some `I` that implements [`Info`].
        pub type Rhs<I> = <<I as Info>::Veritable as Veritable>::GenericRhs;
    }

    /// Helpers to get the associated types for the trait.
    pub mod associated {
        use super::*;

        /// Get the `Output` associated type for some `I` that implements [`Info`].
        pub type Output<I> = <<I as Info>::Veritable as Veritable>::AssociatedOutput;
    }

    /// Info for a trait bound that knows about [`std::ops::Add`].
    pub trait Info: ForType + Send + Sync + Unpin + RefUnwindSafe + UnwindSafe {
        /// The veritable describing [`std::ops::Add`] and how `T` implements it.
        type Veritable: Veritable<T = <Self as ForType>::T>;
    }

    /// Describes how `<Self as ForType>::T` implements [`std::ops::Add`].
    pub trait Veritable:
        ForType + IsRequired + Send + Sync + Unpin + RefUnwindSafe + UnwindSafe
    {
        /// The type for the `Rhs` generic.
        type GenericRhs;

        /// The type for the `Associated` generic.
        type AssociatedOutput;

        /// The implemention of [`std::ops::Add::add`].
        ///
        /// # Safety
        /// This method must only be called if `<<Self as IsRequire>::Required as Bool>::VALUE == true`.
        unsafe fn unchecked_add(
            this: <Self as ForType>::T,
            rhs: <Self as Veritable>::GenericRhs,
        ) -> <Self as Veritable>::AssociatedOutput;
    }

    /// Safe form of [`Veritable`].
    ///
    /// This is implemented when `<Self as IsRequire>::Required = True`.
    pub trait SafeVeritable: Veritable {
        fn add(
            this: <Self as ForType>::T,
            rhs: <Self as Veritable>::GenericRhs,
        ) -> <Self as Veritable>::AssociatedOutput;
    }

    /// VTable form of [`Veritable`].
    ///
    /// The vtable is able to lower type level knowledge into runtime knowledge.
    #[derive(Debug)]
    pub struct VTable<
        This,             /*: ?Sized*/
        GenericRhs,       /*: ?Sized*/
        AssociatedOutput, /*: ?Sized*/
    > {
        unchecked_add: unsafe fn(This, GenericRhs) -> AssociatedOutput,
        required: bool,
    }

    impl<This, GenericRhs, AssociatedOutput> Clone for VTable<This, GenericRhs, AssociatedOutput> {
        fn clone(&self) -> Self {
            *self
        }
    }

    impl<This, GenericRhs, AssociatedOutput> Copy for VTable<This, GenericRhs, AssociatedOutput> {}

    /// Construct the [`VTable`] for a `I` implementing [`Info`].
    ///
    /// The returned [`VTable`] has the same type independant of if the `I` actually required
    /// `This` to implement [`std::ops::Add`].
    pub const fn vtable<I: Info>() -> VTableFor<I>
    where
        This<I>: Sized,
    {
        // The veritable has all the info needed to build the vtable.
        VTable {
            unchecked_add: VeritableFor::<I>::unchecked_add,
            required: <VeritableFor<I> as IsRequired>::Required::VALUE,
        }
    }

    impl<
            This,             /*: ?Sized*/
            GenericRhs,       /*: ?Sized*/
            AssociatedOutput, /*: ?Sized*/
        > VTable<This, GenericRhs, AssociatedOutput>
    {
        /// Check if `This` was required to implement [`std::ops::Add`].
        pub const fn was_required(&self) -> bool {
            self.required
        }

        /// The implemention of [`std::ops::Add::add`].
        ///
        /// # Safety
        /// This method must only be called if `self.was_required() == true`.
        pub unsafe fn unchecked_add(&self, this: This, rhs: GenericRhs) -> AssociatedOutput {
            (self.unchecked_add)(this, rhs)
        }

        /// Safe form of [`Self::unchecked_add`].
        ///
        /// If `self.was_required() == false`, then `None` is returned.
        pub fn checked_add(&self, this: This, rhs: GenericRhs) -> Option<AssociatedOutput> {
            if self.required {
                unsafe { Some((self.unchecked_add)(this, rhs)) }
            } else {
                None
            }
        }
    }

    /// Implementor of [`Veritable`].
    pub struct Impl<T: ?Sized, Required: Bool, GenericRhs: ?Sized, AssociatedOutput: ?Sized> {
        _marker: PhantomData<(
            fn() -> (
                Required,
                *const T,
                *const GenericRhs,
                *const AssociatedOutput,
            ),
        )>,
    }

    impl<
            T, /*: ?Sized*/
            Required: Bool,
            GenericRhs,       /*: ?Sized*/
            AssociatedOutput, /*: ?Sized*/
        > Info for Impl<T, Required, GenericRhs, AssociatedOutput>
    where
        Self: Veritable<GenericRhs = GenericRhs, T = T>,
    {
        type Veritable = Self;
    }

    impl<T: ?Sized, Required: Bool, GenericRhs: ?Sized, AssociatedOutput: ?Sized> ForType
        for Impl<T, Required, GenericRhs, AssociatedOutput>
    {
        type T = T;
    }

    impl<T: ?Sized, Required: Bool, GenericRhs: ?Sized, AssociatedOutput: ?Sized> IsRequired
        for Impl<T, Required, GenericRhs, AssociatedOutput>
    {
        type Required = Required;
    }

    impl<T: ::core::ops::Add<GenericRhs> /* + ?Sized*/, GenericRhs /*?Sized*/> Veritable
        for Impl<T, True, GenericRhs, <T as ::core::ops::Add<GenericRhs>>::Output>
    {
        type GenericRhs = GenericRhs;

        type AssociatedOutput = <T as ::core::ops::Add<GenericRhs>>::Output;

        unsafe fn unchecked_add(
            this: <Self as ForType>::T,
            rhs: GenericRhs,
        ) -> Self::AssociatedOutput {
            <T as ::core::ops::Add<GenericRhs>>::add(this, rhs)
        }
    }

    impl<T /*: ?Sized*/, GenericRhs /*: ?Sized*/, AssociatedOutput /*: ?Sized*/> Veritable
        for Impl<T, False, GenericRhs, AssociatedOutput>
    {
        type GenericRhs = GenericRhs;

        type AssociatedOutput = AssociatedOutput;

        #[allow(unused_variables)]
        unsafe fn unchecked_add(
            this: <Self as ForType>::T,
            rhs: GenericRhs,
        ) -> Self::AssociatedOutput {
            unimplemented!()
        }
    }

    impl<V: Veritable> SafeVeritable for V
    where
        <V as ForType>::T: Sized,
        V: IsRequired<Required = True>,
    {
        fn add(
            this: <Self as ForType>::T,
            rhs: <V as Veritable>::GenericRhs,
        ) -> <Self as Veritable>::AssociatedOutput {
            unsafe { <V as Veritable>::unchecked_add(this, rhs) }
        }
    }
}
